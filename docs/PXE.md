# Configuring a PXE boot environment

In this example we will use a Linux VM with 2 NIC’s. One on the regular network (set to bridged mode, `eth0`) and one set to private (Internal Network, `eth1`)

Ubuntu was used for these instructions. Add dhcp3-server and tftpd-hpa.

Open a command prompt for the following instructions:

`sudo apt-get install isc-dhcp-server tftpd-hpa`

In Debian/Ubuntu the config for tftpd is located at `/etc/default/tftpd-hpa`. In Fedora/Redhat the config for tftpd is located at `/etc/xinetd.d/tftp`

Configure `eth1` to a manual IP (192.168.242.1, 255.255.255.0)

`sudo nano -w /etc/default/isc-dhcp-server`

Add the interface name in “INTERFACESv4”. Enter `eth1`

`sudo nano -w /etc/dhcp/dhcpd.conf`

Add the following:

```
subnet 192.168.242.0 netmask 255.255.255.0 {
    range 192.168.242.10 192.168.242.159;
    filename “pxeboot.bin”;
}
```

Now start the DHCP server:

`sudo service isc-dhcp-server restart`

At this point we can verify if the DCHP and TFTP services are running correctly. Create a new VM within VirtualBox with no Hard Drive. Configure a single NIC to be on the private network and set the boot order to Network first.