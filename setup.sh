#!/bin/bash

mkdir -p sys
test -d src || mkdir src
cd src
test -d Pure64    || git clone https://gitlab.com/ReturnInfinity/Pure64.git
test -d BareMetal || git clone https://gitlab.com/ReturnInfinity/BareMetal.git
cd ..
./build.sh
